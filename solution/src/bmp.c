#include "../include/bmp.h"

#define OFFSET 54
#define SIGNATURE 0x4D42
#define INFO_SIZE 40
#define PLANES 1
#define BIT_COUNT 24

static uint8_t calculate_padding(uint64_t const width){
    return (4 - (uint8_t)(( width*sizeof(struct pixel))%4));
}
static struct bmp_header bmp_header_initialize(uint64_t width, uint64_t height) {
    uint64_t image_size = width * height * sizeof(struct pixel);
    uint64_t file_size = image_size + sizeof(struct bmp_header);
    return (struct bmp_header) {
            .bfType = SIGNATURE,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bOffBits = OFFSET,
            .biSize = INFO_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

static enum read_status validate_header(struct bmp_header const header) {
    if (header.bfType != SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != BIT_COUNT) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

static enum read_status read_header(struct bmp_header* const header, FILE * const input){
    if (!(fread(header, sizeof( struct bmp_header ),1,input))){
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

static enum write_status write_header( struct bmp_header const * const header, FILE* const output_file){
    if (header!=NULL){
        if (fwrite(header, sizeof(struct bmp_header), 1, output_file) == 1){
            return WRITE_OK;
        }
    }
    return WRITE_ERROR;
}

enum write_status to_bmp(FILE* const output, struct image const image) {
    struct bmp_header header = bmp_header_initialize(image.width, image.height);
    if (write_header(&header, output) != WRITE_OK){
        return WRITE_ERROR;
    }
    uint8_t padding = calculate_padding(image.width);
    for (uint64_t i = 0; i < image.height; i++) {
        if (fwrite(image.data + i * image.width, sizeof(struct pixel), image.width, output) != image.width) {
            return WRITE_ERROR;
        }
        if (fseek(output, padding, SEEK_CUR) == -1) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

enum read_status from_bmp(FILE* const input, struct image* const image) {
    struct bmp_header header = {0};
    enum read_status status = read_header(&header, input);
    if (status!=READ_OK) {
        return status;
    }
    enum read_status readStatus = validate_header(header);
    if (readStatus!=READ_OK){
        return readStatus;
    }
    *image = image_initialize(header.biWidth, header.biHeight);
    for (uint64_t i = 0; i < image->height; i++) {
        if (!fread(image->data + i * image->width, image->width * sizeof(struct pixel), 1, input)) {
            free(image->data);
            return READ_INVALID_BITS;
        }
        if (fseek(input, calculate_padding(image->width), SEEK_CUR) == -1) {
            free(image->data);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}
