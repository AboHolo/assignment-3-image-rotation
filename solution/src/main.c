#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/rotate.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "There must be 3 arguments");
        return 1;
    }

    FILE* input = NULL;

    if (file_open(&input, argv[1], "rb")) {
        fprintf(stderr, "Failed to open the input file");
        return 1;
    }

    FILE* output = NULL;
    if (file_open(&output, argv[2], "wb")) {
        fprintf(stderr, "Failed to open the output file");
        file_close(input);
        return 1;
    }

    struct image image = {0};
    struct image new_image;
    if (from_bmp(input, &image)!=READ_OK){
        fprintf(stderr,"Failed to read the input file ");
        file_close(input);
        file_close(output);
        return 1;
    }

    new_image = rotate(image);
    if (to_bmp(output, new_image)!=WRITE_OK) {
        fprintf(stderr, "Failed to write to the output file");
        image_destroy(image);
        image_destroy(new_image);
        file_close(input);
        file_close(output);
    }

    enum file_close_status close_status;
    close_status = file_close(input);
    if (close_status) {
        fprintf(stderr, "Error occurred when closing input file");
    }
    close_status = file_close(output);
    if (close_status) {
        fprintf(stderr,"Error occurred when closing output file");
    }
    printf("Successfully created file.");
    image_destroy(image);
    image_destroy(new_image);
    return 0;
}

