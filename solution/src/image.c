#include "../include/image.h"

struct image image_initialize(uint64_t const width, uint64_t const height) {
    struct image image = {
            .height = height,
            .width = width,
            .data = malloc(width * height * sizeof(struct pixel))
    };
    return image;
}

void image_destroy(struct image const image) {
    free(image.data);
}

struct pixel image_get_pixel(struct image const image, uint64_t const offset, uint64_t const row) {
    return image.data[image.width * row + offset];
}

void image_set_pixel(struct image const image, uint64_t const offset, uint64_t const row, struct pixel const pixel) {
    image.data[image.width * row + offset] = pixel;
}

