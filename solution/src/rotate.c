#include "../include/image.h"
#include "../include/rotate.h"

struct image rotate( struct image const source ) {
    struct image rotated_image = image_initialize(source.height, source.width);
    for (uint64_t row = 0; row < source.height; row++) {
        for (uint64_t offset = 0; offset < source.width; offset++) {
            image_set_pixel(rotated_image, rotated_image.width - row - 1, offset, image_get_pixel(source, offset, row));
        }
    }
    return rotated_image;
}

